<?xml version="1.0" encoding="UTF-8" ?>
<Package name="lab2" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="zad1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="zad2" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="zad3" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="zad4" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="zad5" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="choice_sentences" src="zad5/Aldebaran/choice_sentences.xml" />
        <File name="choice_sentences" src="zad2/Aldebaran/choice_sentences.xml" />
    </Resources>
    <Topics />
    <IgnoredPaths />
</Package>
